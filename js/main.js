$(document).ready(function(){

    // Menu nav
    $('.menu__nav').on('click', function(event){
        let dataId = $(event.target).data('id');
        $(document.documentElement).animate({
            scrollTop: $(`section[data-id="${dataId}"]`).offset().top
        }, 1000);
    });

    // Our Services
    $('.tabs').on('click', function(event){
        event.preventDefault();
        $('.tabs .active').removeClass('active');
        $(event.target).addClass('active');
        let idx = $(event.target).index();
        $('.tabs-content .active').removeClass('active');
        $('.tabs-content .tab').eq(idx).addClass('active');
    });

    // Amazing
    $('.amazing .amazing__tabs-content').html('');
    function getImages(multy, callback){
        $.getJSON('./data/img.json', function (data) {
            const listItems = data.map(function(img, index){
                return `<li class="amazing__tab" data-category="${img['category']}"><img src="${img['src'] + '?' + index*multy}" class="amazing__img">
                <div class="amazing__block">
                        <div class="amazing__block-circles">
                            <div class="amazing__block-circle">
                                <i class="fas fa-link fa-sm" aria-hidden="true"></i>
                            </div>
                            <div class="amazing__block-circle">
                                <i class="fas fa-search fa-sm" aria-hidden="true"></i>
                            </div>
                        </div>

                        <div class="amazing__block-creative">creative design</div>
                        <div class="amazing__block-design">${img['name']}</div>
                    </div>
            </li>`;
            }).join('');

            $('.amazing .amazing__tabs-content').append(listItems);
            callback && callback();
        });
    }

    getImages(1);


    // btn__load

    $('.amazing .btn__load').on('click', function(event){
        event.preventDefault();
        $(this).remove();


        getImages(2, function() {
            $('.amazing__tab.hidden').removeClass('hidden');
            const category = $('.amazing__tabs-title.active').data('category');

            if(category !== 'all') {
                $(`.amazing__tab[data-category]:not([data-category="${category}"])`).addClass('hidden');
            }
        });

    });


    // amazing__tabs

    $('.amazing__tabs').on('click', function(event){
        event.preventDefault();
        $('.amazing__tabs .active').removeClass('active');
        $(event.target).addClass('active');
        let category = $(event.target).data('category');

        $('.amazing__tab.hidden').removeClass('hidden');

        if(category !== 'all')  {
            $(`.amazing__tab[data-category]:not([data-category="${category}"])`).addClass('hidden');
        }

    });



    // Breaking News
    $('.news__article').on('click', function(event){
        event.preventDefault();
    });

    // Testimonials/ Carousel
    const names = [
        {
            name: 'Eva Green',
            position: 'Developer'
        },
        {
            name: 'HELEN BROWN',
            position: 'Architect'
        },
        {
            name: 'Vinona Red',
            position: 'UX Designer'
        },
        {
            name: 'Victoria Black',
            position: 'QA Engineer'
        }
    ];

    const $testimonials = $('.testimonials');
    const $employee = $('.employee');
    const $carousel = $testimonials.find('.carousel');
    const $slider = $carousel.find('.slider');


    const intervalCallback = () => $carousel.find('.fa-chevron-right').click();
    let interval = setInterval(intervalCallback, 3000);


    $slider.on('click', function(event) {
        const $tg = $(event.target);

        if($tg.is('.carousel__img')) {
            clearInterval(interval);

            $slider.find('.active').removeClass('active');
            $tg.addClass('active');
            $employee.find('.employee__img').attr('src', $tg.attr('src'));

            interval = setInterval(intervalCallback, 3000);
        }
    });

    $carousel.on('click', function(event) {
        const $tg = $(event.target);

        if($tg.is('.carousel__arrow')) {
            let $result = $slider.find('.active').removeClass('active');

            clearInterval(interval);

            if($tg.is('.fa-chevron-left')) {
                $result = $result.prev();

                if(!$result.length) {
                    $result = $slider.children().last();
                }

                $result.addClass('active');

            } else {
                $result = $result.next();

                if(!$result.length) {
                    $result = $slider.children().first();
                }

                $result.addClass('active');
            }

            if(!event.originalEvent) {
                $employee.animate({
                    opacity: 0
                }, {
                    duration: 500,
                    complete: () => {
                        $employee.find('.employee__name').text(names[$result.index()].name);
                        $employee.find('.employee__position').text(names[$result.index()].position);
                        $employee.find('.employee__img').attr('src', $slider.find('.active').attr('src'));
                    }
                }).animate({
                    opacity: 1,
                }, 500);
            } else {
                $employee.find('.employee__name').text(names[$result.index()].name);
                $employee.find('.employee__position').text(names[$result.index()].position);
                $employee.find('.employee__img').attr('src', $slider.find('.active').attr('src'));
            }



            interval = setInterval(intervalCallback, 3000);
        }
    });

});